#!/usr/bin/env sh

# network
docker network create tests

# standalone chrome
# docker run -d -p 4444:4444 --shm-size="2g" selenium/standalone-chrome:4.1.1-20211217
docker run --rm -d --name chrome \
  --network tests \
  -p4444:4444 \
  selenium/standalone-chrome:4.1.1-20211217
