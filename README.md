# About

QA automation coding challenge(s).

## Tested with

* JDK `11.0.13` and `apache-maven-3.6.3`
* JDK `11` and `apache-maven-3.8.4`
* JDK `17.0.1` and `apache-maven-3.8.4`

# Running

The surefire and failsafe Maven plugins are used to run Unit (UT) and Integration Tests (IT) automatically. You can also run the
tests through an IDE. This was tested with IntelliJ IDEA 2021.3.

## Gitlab

A `.gitlab-ci.yml` file exists in the root to inform the Gitlab pipeline to spin up a standalone Chrome browser as a
service, then run maven to execute tests.

## Locally

> Note: Docker is a prerequisite. It is needed for the UI/Selenium tests to work. It will be used to spin up a
> standalone Chrome browser to run Selenium tests against. Docker is also used to pin a specific, known, working
> version of Maven and the JDK.

To simulate what is run in Gitlab, execute the following shell script:

    ./run_tests.sh

The above essentially starts up a Browser/WebDriver via Docker (`start_webdriver.sh`), runs maven, then spins the
container back down (`stop_webdriver.sh`).

For development or troubleshooting, you can manually start the container:

    ./start_webdriver.sh

... run your tests via IDE (or `mvn clean verify`) ...

then manually stop the WebDriver container when you are done:

    ./stop_webdriver.sh


# Troubleshooting
Screenshots are saved in the temporary maven auto-generated `target` subdirectory. They are taken on demand within an
Integration Test, or automatically during certain `Exception`s. Check there for clues.  Often times, the UI failure is
due to a CAPTCHA popup.

To workaround the CAPTCHA, you can temporarily disable one of the UI IT classes, and only run a single UI IT at a time.

You can also try the local Firefox of Chrome WebDrivers to visually see, step-by-step, where the test is failing. Find
the different WebDrivers in `WebDriverFactory` and update the IT's `@BeforeMethod` appropriately. You will need the
appropriate WebDriver binary installed and path updated in `WebDriverFactory`.

Disclaimer: not all browsers/configuration work for this MVP.

# References
* https://github.com/SeleniumHQ/docker-selenium
