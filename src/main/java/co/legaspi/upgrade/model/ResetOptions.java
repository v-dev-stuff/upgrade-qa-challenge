package co.legaspi.upgrade.model;

public enum ResetOptions {
    LEAD_SECRET,
    WIPE,
    DEACTIVATE_USER
}
