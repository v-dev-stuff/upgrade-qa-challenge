package co.legaspi.upgrade.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse {
    private long code;
    private String codeName;
    private String message;
    private boolean retryable;
    private String type;
    private String httpStatus;
}
