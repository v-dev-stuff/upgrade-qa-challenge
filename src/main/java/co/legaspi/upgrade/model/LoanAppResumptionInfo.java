package co.legaspi.upgrade.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanAppResumptionInfo {
    private String loanAppId;
    private String loanAppUuid;
    private String referrer;
    private String status;
    private String productType;
    private BorrowerResumptionInfo borrowerResumptionInfo;
}
