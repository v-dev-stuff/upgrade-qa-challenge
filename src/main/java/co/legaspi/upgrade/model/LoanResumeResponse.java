package co.legaspi.upgrade.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoanResumeResponse {
    private LoanAppResumptionInfo loanAppResumptionInfo;
    private List<ResetOptions> resetOptions;
}
