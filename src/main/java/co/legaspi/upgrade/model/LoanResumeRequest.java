package co.legaspi.upgrade.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoanResumeRequest {
    public static final String VALID_LOAN_APP_UUID = "b8096ec7-2150-405f-84f5-ae99864b3e96";
    private String loanAppUuid;
    private boolean skipSideEffects;

    public static class LoanResumeRequestBuilder {
        private String loanAppUuid = VALID_LOAN_APP_UUID;
        private boolean skipSideEffects = true;

        public LoanResumeRequestBuilder defaults() {
            return this;
        }
    }
}
