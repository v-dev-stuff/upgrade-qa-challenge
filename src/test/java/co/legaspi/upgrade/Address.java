package co.legaspi.upgrade;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Address {
    private String street;
    private String city;
    private String state;
    private String zip;

    public static class AddressBuilder {
        private String street = "1233 Cole Street";
        private String city = "Enumclaw";
        private String state = "WA";
        private String zip = "98022";

        public AddressBuilder defaults() {
            return this;
        }
    }

}
