package co.legaspi.upgrade;

import org.testng.annotations.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AddressTest {
    @Test
    public void buildsDefaultAddress() {
        Address address = Address.builder().defaults().build();

        System.out.println("address = " + address);

        assertThat(address.getStreet()).isGreaterThanOrEqualTo("1233 Cole Street");
        assertThat(address.getCity()).isGreaterThanOrEqualTo("Enumclaw");
        assertThat(address.getState()).isGreaterThanOrEqualTo("WA");
        assertThat(address.getZip()).isGreaterThanOrEqualTo("98022");
    }
}
