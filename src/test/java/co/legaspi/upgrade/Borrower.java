package co.legaspi.upgrade;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Borrower {
    // not really a "borrower" property, but keeping it here for MVP
    private String desiredAmount;
    private String firstName;
    private String lastName;
    private int individualIncome;
    private int additionalIncome;
    private String email;
    private String password;
    private String dateOfBirth;
    private Address address;

    public static class BorrowerBuilder {
        private String desiredAmount = Utils.randomLoanAmount();
        private String firstName = Utils.randomName();
        private String lastName = Utils.randomName();
        private int individualIncome = Utils.randomInt(120_000, 130_000);
        private int additionalIncome = Utils.randomInt(5_000, 6_000);
        private String email = Utils.randomEmail();
        private String password = Utils.randomPassword();
        private String dateOfBirth = "09/09/1999";
        private Address address = Address.builder().defaults().build();

        public BorrowerBuilder defaults() {
            return this;
        }

    }

}
