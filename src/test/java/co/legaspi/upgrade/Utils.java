package co.legaspi.upgrade;

import java.time.Instant;
import java.util.Random;
import java.util.UUID;

public class Utils {
    private Utils() {
        // Don't instantiate a purely utils class with all static methods
    }

    public static String randomUuid() {
        return UUID.randomUUID().toString();
    }

    /**
     * @param min inclusive
     * @param max exclusive
     */
    public static int randomInt(int min, int max) {
        Random rand = new Random();
        return rand.ints(min, max).findFirst().getAsInt();
    }

    /**
     * @return random integer between 1 and Integer.MAX_VALUE - 1
     * Note: possibly better to use epoch or some time stamp to increase uniqueness
     */
    public static int randomInt() {
        return randomInt(1, Integer.MAX_VALUE);
    }

    /**
     * candidate+<random-number>@upgrade-challenge.com
     */
    public static String randomEmail() {
        return "candidate+" + randomInt() + "@upgrade-challenge.com";
    }

    /**
     * Strong password: 8 characters long, contain at least one number, one uppercase letter, and one lower case letter.
     */
    public static String randomPassword() {
        return "1A" + randomAlphabetic(6, 7).substring(0, 6);
    }

    /**
     * using personal loan min & max values
     * min of $2,000
     * max of $3,000
     *
     * @return a random value, in String between 2000 and 50000
     */
    public static String randomLoanAmount() {
        return String.valueOf(randomInt(2000, 3000));
    }

    /**
     * mostly copied from https://www.baeldung.com/java-random-string
     * At least 3 chars long
     * Max 15 chars long
     */
    public static String randomAlphabetic(int min, int max) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int length = randomInt(min, max);

        return new Random().ints(leftLimit, rightLimit + 1)
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    /**
     * At least 3 chars long
     * Max 15 chars long
     */
    public static String randomName() {
        return randomAlphabetic(3, 16);
    }

    public static String getEpoch() {
        return String.valueOf(Instant.now().toEpochMilli());
    }

}
