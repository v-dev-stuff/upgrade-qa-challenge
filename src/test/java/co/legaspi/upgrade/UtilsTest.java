package co.legaspi.upgrade;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class UtilsTest {

    @Test
    public void testRandomUuid() {
        String actual = Utils.randomUuid();
        int expectedUuidLength = 36;
        assertThat(actual.length()).isEqualTo(expectedUuidLength);
        assertThat(actual).contains("-");
    }

    @Test
    public void testRandomInt() {
        for (int i = 0; i < 50; i++) {
            int actual = Utils.randomInt();
            assertThat(actual).isGreaterThan(0);
            assertThat(actual).isLessThan(Integer.MAX_VALUE);
        }
    }

    @Test
    public void testRandomEmail() {
        String actual = Utils.randomEmail();
        assertThat(actual).contains("candidate+");
        assertThat(actual).contains("@upgrade-challenge.com");
        assertThat(actual.length()).isGreaterThan("candidate+@upgrade-challenge.com".length());
    }

    @Test
    public void testRandomName() {
        for (int i = 0; i < 50; i++) {
            String actual = Utils.randomName();
            assertThat(actual.length()).isGreaterThanOrEqualTo(3);
            assertThat(actual.length()).isLessThan(16);
        }
    }

    @Test
    public void testRandomPassword() {
        for (int i = 0; i < 50; i++) {
            String actual = Utils.randomPassword();
            assertThat(actual).startsWith("1A");
            assertThat(actual.length()).isEqualTo(8);
        }
    }
}
