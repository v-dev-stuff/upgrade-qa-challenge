package co.legaspi.upgrade.ui;

import co.legaspi.upgrade.Borrower;
import co.legaspi.upgrade.ui.pageobject.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.MalformedURLException;

import static org.assertj.core.api.Assertions.assertThat;

public class LoanApplicationIT {

    WebDriver driver;

    @BeforeMethod
    public void before() throws MalformedURLException {
        driver = WebDriverFactory.useRemoteChrome();
    }

    @Test
    public void fullCreditCardFlow() throws IOException {
        Borrower borrower = Borrower.builder().defaults().build();
        System.out.println("The following Borrower will be used...");
        System.out.println(borrower + System.lineSeparator());

        GetStartedPage start = new GetStartedPage(driver);
        start.go();
        start.enterDesiredAmount(borrower.getDesiredAmount());
        start.selectPurpose(LoanPurpose.CREDIT_CARD);
        start.takeScreenShot("purpose-selected");

        BasicInfoPage basicInfo = start.clickContinue();
        basicInfo.waitForPageToLoad();
        basicInfo.takeScreenShot("basicinfopage");
        basicInfo.enterBorrowerInfo(borrower);
        basicInfo.takeScreenShot("entered-info");

        IncomePage income = basicInfo.clickContinue();
        income.enterIndividualIncome(String.valueOf(borrower.getIndividualIncome()));
        income.enterAdditionalIncome(String.valueOf(borrower.getAdditionalIncome()));
        income.takeScreenShot("entered-income");

        CreateAccountPage account = income.clickContinue();

        OfferPage offer = account.enterCredsAndGetRate(borrower);
        offer.waitForPageToLoad();
        offer.takeScreenShot("offer-page");

        OfferDetails initialOffer = offer.getOfferDetails();
        System.out.println("initialOffer = " + initialOffer);

        LogoutPage logout = offer.signOut();
        offer.takeScreenShot("signout");

        String logoutMessage = logout.getLogoutMessage();
        assertThat(logoutMessage).contains("You've been successfully logged out.");
        assertThat(logoutMessage).contains("See you later.");

        // log back in & verify same data
        LoginPage login = new LoginPage(driver);
        login.go();
        login.enterEmail(borrower.getEmail());
        login.enterPassword(borrower.getPassword());
        login.takeScreenShot("login-filled");
        OfferPage resumedOfferPage = login.clickSignIn();

        OfferDetails resumedOfferDetails = resumedOfferPage.getOfferDetails();
        System.out.println("resumedOfferDetails = " + resumedOfferDetails);
        assertThat(resumedOfferDetails).isEqualTo(initialOffer);

        // #8: "Make sure you are on /offer-page":
        resumedOfferPage.takeScreenShot("second-offer");
        String secondOfferUrl = resumedOfferPage.getUrl();
        assertThat(secondOfferUrl).contains("offer-page");
    }

    @AfterMethod
    public void afterMethod() {
        driver.quit();
    }

    @AfterClass
    public void afterClass() {
        driver.quit();
    }

}
