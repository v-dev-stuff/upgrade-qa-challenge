package co.legaspi.upgrade.ui;

import co.legaspi.upgrade.Utils;
import co.legaspi.upgrade.ui.pageobject.BasicInfoPage;
import co.legaspi.upgrade.ui.pageobject.GetStartedPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.MalformedURLException;

public class LoanPurposeIT {
    WebDriver driver;

    @BeforeMethod
    public void before() throws MalformedURLException {
        driver = WebDriverFactory.useRemoteChrome();
    }

    @Test(dataProvider = "nonBusinessPurposes")
    public void allNonBusinessPurposesProceedsToNextPage(LoanPurpose purpose) throws IOException {
        String desiredAmount = Utils.randomLoanAmount();
        GetStartedPage start = new GetStartedPage(driver);
        start.go();

        start.enterDesiredAmount(desiredAmount);
        start.selectPurpose(purpose);
        start.takeScreenShot(purpose + "-selected");
        BasicInfoPage basicInfo = start.clickContinue();
        basicInfo.waitForPageToLoad();
        basicInfo.takeScreenShot(purpose + "_basicinfopage");
    }

    @DataProvider(name = "nonBusinessPurposes")
    public Object[][] nonBusinessPurposes() {
        LoanPurpose[] purposes = LoanPurpose.values();
        Object[][] data = new Object[purposes.length - 1][];
        int i = 0;
        for (LoanPurpose purpose : purposes) {
            if (purpose.equals(LoanPurpose.SMALL_BUSINESS)) {
                continue;
            }
            data[i] = new Object[]{purpose};
            i++;
        }
        return data;
    }

    @Test(enabled = false)
    public void smallBusinessHasPopup() {
        // TODO
    }

    @AfterMethod
    public void afterMethod() {
        driver.quit();
    }

    @AfterClass
    public void afterClass() {
        driver.quit();
    }

}
