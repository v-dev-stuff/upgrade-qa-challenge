package co.legaspi.upgrade.ui;

public enum LoanPurpose {
    CREDIT_CARD,
    DEBT_CONSOLIDATION,
    SMALL_BUSINESS,
    HOME_IMPROVEMENT,
    LARGE_PURCHASE,
    OTHER
}
