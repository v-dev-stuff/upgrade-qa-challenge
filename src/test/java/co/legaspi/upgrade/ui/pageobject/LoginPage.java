package co.legaspi.upgrade.ui.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {
    static final String LOGIN_URL = "https://www.credify.tech/portal/login";
    static final By EMAIL_LOCATOR = By.name("username");
    static final By PASSWORD_LOCATOR = By.name("password");
    static final By SIGNIN_BUTTON_LOCATOR = By.cssSelector("button[data-auto='login']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage go() {
        driver.get(LOGIN_URL);
        return this;
    }

    public void enterEmail(String email) {
        findClearAndEnter(EMAIL_LOCATOR, email, true);
    }

    public void enterPassword(String password) {
        findClearAndEnter(PASSWORD_LOCATOR, password, true);
    }

    public OfferPage clickSignIn() {
        locateElement(SIGNIN_BUTTON_LOCATOR).click();
        return new OfferPage(driver);
    }
}
