package co.legaspi.upgrade.ui.pageobject;

import co.legaspi.upgrade.ui.LoanPurpose;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

/**
 * https://www.credify.tech/funnel/nonDMFunnel
 */
public class GetStartedPage extends BasePage {
    static final String GET_STARTED_URL = "https://www.credify.tech/funnel/nonDMFunnel";
    static final By DESIRED_AMOUNT_LOCATOR = By.name("desiredAmount");
    static final By LOAN_PURPOSE_LOCATOR = By.name("loan-purpose");

    public GetStartedPage(WebDriver driver) {
        super(driver);
    }

    public GetStartedPage go() {
        driver.get(GET_STARTED_URL);
        return this;
    }

    public GetStartedPage enterDesiredAmount(String amount) {
        return (GetStartedPage) findClearAndEnter(DESIRED_AMOUNT_LOCATOR, amount, true);
    }

    public GetStartedPage selectPurpose(LoanPurpose purpose) {
        Select loanPurpose = new Select(wait.until(presenceOfElementLocated(LOAN_PURPOSE_LOCATOR)));
        loanPurpose.selectByValue(purpose.toString());
        return this;
    }

    public BasicInfoPage clickContinue() {
        scrollToAndClickButton();
        return new BasicInfoPage(driver);
    }

}
