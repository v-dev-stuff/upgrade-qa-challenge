package co.legaspi.upgrade.ui.pageobject;

import co.legaspi.upgrade.Borrower;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BasicInfoPage extends BasePage {
    static final By FIRST_NAME_LOCATOR = By.name("borrowerFirstName");
    static final By LAST_NAME_LOCATOR = By.name("borrowerLastName");
    static final By ADDRESS_LOCATOR = By.name("borrowerStreet");
    static final By CITY_LOCATOR = By.name("borrowerCity");
    static final By STATE_LOCATOR = By.name("borrowerState");
    static final By ZIP_LOCATOR = By.name("borrowerZipCode");
    static final By DOB_LOCATOR = By.name("borrowerDateOfBirth");

    public BasicInfoPage(WebDriver driver) {
        super(driver);
    }

    public void enterBorrowerInfo(Borrower borrower) {
        enterFirstName(borrower.getFirstName());
        enterLastName(borrower.getLastName());
        enterAddress(borrower.getAddress().getStreet());
        enterCity(borrower.getAddress().getCity());
        enterState(borrower.getAddress().getState());
        enterZip(borrower.getAddress().getZip());
        enterDob(borrower.getDateOfBirth());
    }

    public void waitForPageToLoad() {
        By textLocator = By.cssSelector("h1.title--secondary.section");
        try {
            wait.until(ExpectedConditions.textToBePresentInElementLocated(textLocator, "Let's get started with some basic information"));
        } catch (Exception e) {
            captureExceptionAndScreenshot(e);
        }
    }

    public BasicInfoPage enterFirstName(String firstName) {
        return (BasicInfoPage) findClearAndEnter(FIRST_NAME_LOCATOR, firstName, true);
    }

    public BasicInfoPage enterLastName(String lastName) {
        return (BasicInfoPage) findClearAndEnter(LAST_NAME_LOCATOR, lastName, true);
    }

    public BasicInfoPage enterAddress(String address) {
        return (BasicInfoPage) findClearAndEnter(ADDRESS_LOCATOR, address, true);
    }

    public BasicInfoPage enterCity(String city) {
        return (BasicInfoPage) findClearAndEnter(CITY_LOCATOR, city, true);
    }

    public BasicInfoPage enterState(String state) {
        return (BasicInfoPage) findClearAndEnter(STATE_LOCATOR, state, true);
    }

    public BasicInfoPage enterZip(String zip) {
        return (BasicInfoPage) findClearAndEnter(ZIP_LOCATOR, zip, true);
    }

    public BasicInfoPage enterDob(String dob) {
        return (BasicInfoPage) findClearAndEnter(DOB_LOCATOR, dob, false);
    }

    public IncomePage clickContinue() {
        scrollToAndClickButton();
        return new IncomePage(driver);
    }

}
