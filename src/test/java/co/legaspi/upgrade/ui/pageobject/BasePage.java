package co.legaspi.upgrade.ui.pageobject;

import co.legaspi.upgrade.Utils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Collections;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfAllElementsLocatedBy;

public class BasePage {
    static final By BUTTON_LOCATOR = By.tagName("button");

    protected WebDriver driver;
    protected WebDriverWait wait;

    static final int MAX_WAIT_IN_SECONDS = 20;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(MAX_WAIT_IN_SECONDS));
    }

    public BasePage findClearAndEnter(By locator, String string, boolean useTab) {
        WebElement lastNameInput = locateElement(locator);
        lastNameInput.clear();
        lastNameInput.sendKeys(string);
        if (useTab) {
            lastNameInput.sendKeys(Keys.TAB);
        }
        return this;
    }

    public BasePage scrollToAndClickButton() {
        WebElement button = locateElement(BUTTON_LOCATOR);
        try {
            Actions actions = new Actions(driver);
            actions.moveToElement(button);
            actions.perform();
        } catch (Exception e) {
            captureExceptionAndScreenshot(e);
        }
        button.click();
        return this;
    }

    public WebElement locateElement(By locator) {
        List<WebElement> elements = Collections.emptyList();
        try {
            elements = wait.until(presenceOfAllElementsLocatedBy(locator));
        } catch (Exception e) {
            captureExceptionAndScreenshot(e);
        }
        return elements.get(0);
    }

    public void captureExceptionAndScreenshot(Exception e) {
        String filename = "exception_" + Utils.getEpoch();
        System.out.printf("Exception thrown: [%s]. Attempting to save as screenshot: [%s]", e.getMessage(), filename);
        try {
            takeScreenShot(filename);
        } catch (IOException ex) {
            System.out.println("Exception taking screenshot: " + e.getMessage());
        }
    }

    public void takeScreenShot(String fileName) throws IOException {
        File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileHandler.copy(src, new File("target/" + fileName + ".png"));
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }
}
