package co.legaspi.upgrade.ui.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * https://www.credify.tech/funnel/nonDMFunnel
 */
public class IncomePage extends BasePage {
    static final By INDIVIDUAL_INCOME_LOCATOR = By.name("borrowerIncome");
    static final By ADDITIONAL_INCOME_LOCATOR = By.name("borrowerAdditionalIncome");

    public IncomePage(WebDriver driver) {
        super(driver);
    }

    public IncomePage enterIndividualIncome(String individual) {
        return (IncomePage) findClearAndEnter(INDIVIDUAL_INCOME_LOCATOR, individual, true);
    }

    public IncomePage enterAdditionalIncome(String additional) {
        return (IncomePage) findClearAndEnter(ADDITIONAL_INCOME_LOCATOR, additional, true);
    }

    public CreateAccountPage clickContinue() {
        scrollToAndClickButton();
        return new CreateAccountPage(driver);
    }

}
