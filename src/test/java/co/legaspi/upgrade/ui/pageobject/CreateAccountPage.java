package co.legaspi.upgrade.ui.pageobject;

import co.legaspi.upgrade.Borrower;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public class CreateAccountPage extends BasePage {
    static final By EMAIL_LOCATOR = By.name("username");
    static final By PASSWORD_LOCATOR = By.name("password");
    static final By AGREE_CHECKBOX_LOCATOR = By.cssSelector("input[type='checkbox']");

    public CreateAccountPage(WebDriver driver) {
        super(driver);
    }

    public OfferPage enterCredsAndGetRate(Borrower borrower) throws IOException {
        enterUsername(borrower.getEmail());
        enterPassword(borrower.getPassword());
        checkAgreeBox();
        takeScreenShot("account-filled");
        return clickCheckYourRate();
    }

    public CreateAccountPage enterUsername(String username) {
        return (CreateAccountPage) findClearAndEnter(EMAIL_LOCATOR, username, true);
    }

    public CreateAccountPage enterPassword(String password) {
        return (CreateAccountPage) findClearAndEnter(PASSWORD_LOCATOR, password, true);
    }

    public CreateAccountPage checkAgreeBox() {
        WebElement box = locateElement(AGREE_CHECKBOX_LOCATOR);
        box.sendKeys(Keys.SPACE);
        return this;
    }

    public OfferPage clickCheckYourRate() {
        scrollToAndClickButton();
        return new OfferPage(driver);
    }
}
