package co.legaspi.upgrade.ui.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * after signing out, taken to "logout" url (https://www.credify.tech/funnel/logout)
 * with "You've been successfully logged out.
 * See you later."
 * <h1 class="title--secondary text--weight-light text--color-primary" data-auto="logoutMessage">You've been successfully logged out.<br>See you later.</h1>
 */
public class LogoutPage extends BasePage {

    static final By LOGOUT_MESSAGE_LOCATOR = By.cssSelector("h1[data-auto='logoutMessage']");

    public LogoutPage(WebDriver driver) {
        super(driver);
    }

    public String getLogoutMessage() {
        WebElement element = locateElement(LOGOUT_MESSAGE_LOCATOR);
        return element.getText();
    }
}
