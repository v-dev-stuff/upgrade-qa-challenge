package co.legaspi.upgrade.ui.pageobject;

import co.legaspi.upgrade.ui.OfferDetails;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class OfferPage extends BasePage {
    static final By LOAN_AMOUNT_LOCATOR = By.cssSelector("span[data-auto='userLoanAmount']");
    static final By MONTHLY_PAYMENT_LOCATOR = By.cssSelector("div[data-auto='defaultMonthlyPayment']");
    static final By TERM_LOCATOR = By.cssSelector("li[data-auto='defaultLoanTerm']");
    static final By INTEREST_RATE_LOCATOR = By.cssSelector("li[data-auto='defaultLoanInterestRate']");
    static final By APR_LOCATOR = By.cssSelector("div[data-auto='defaultAPR']");

    static final By MENU_LOCATOR = By.cssSelector("label[aria-label='Open Site Menu']");
    static final By SIGN_OUT_LOCATOR = By.linkText("Sign Out");

    public OfferPage(WebDriver driver) {
        super(driver);
    }

    public void waitForPageToLoad() {
        By textLocator = By.cssSelector("div.text--align-center.section--lg");
        try {
            wait.until(ExpectedConditions.textToBePresentInElementLocated(textLocator, "Your Loan Amount"));
        } catch (Exception e) {
            captureExceptionAndScreenshot(e);
        }
    }

    public OfferDetails getOfferDetails() {
        OfferDetails offerDetails = new OfferDetails();
        offerDetails.setLoanAmount(getLoanAmount());
        offerDetails.setMonthlyPayment(getMonthlyPayment());
        offerDetails.setTerm(getTerm());
        offerDetails.setRate(getInterestRate());
        offerDetails.setApr(getApr());
        return offerDetails;
    }

    public String getLoanAmount() {
        return locateElement(LOAN_AMOUNT_LOCATOR).getText();
    }

    public String getMonthlyPayment() {
        WebElement webElement = locateElement(MONTHLY_PAYMENT_LOCATOR);
        return webElement.getAttribute("innerText");
    }

    public String getTerm() {
        WebElement webElement = locateElement(TERM_LOCATOR);
        String rawText = webElement.getText();
        String text = rawText.replace("Term", "").replaceAll(System.lineSeparator(), "");
        return text;
    }

    public String getInterestRate() {
        WebElement webElement = locateElement(INTEREST_RATE_LOCATOR);
        String rawText = webElement.getText();
        String text = rawText.replace("Interest Rate", "").replace(System.lineSeparator(), "");
        return text;
    }

    public String getApr() {
        WebElement webElement = locateElement(APR_LOCATOR);
        return webElement.getText();
    }

    public LogoutPage signOut() {
        locateElement(MENU_LOCATOR).click();
        locateElement(SIGN_OUT_LOCATOR).click();
        return new LogoutPage(driver);
    }
}
