package co.legaspi.upgrade.ui;

import lombok.Data;

@Data
public class OfferDetails {
    private String loanAmount;
    private String monthlyPayment;
    private String term;
    private String rate;
    private String apr;
}
