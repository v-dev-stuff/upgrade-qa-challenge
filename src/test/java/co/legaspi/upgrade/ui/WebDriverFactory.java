package co.legaspi.upgrade.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class WebDriverFactory {
    private WebDriverFactory() {
        // Don't instantiate a utils class
    }

    public static WebDriver getLocalChrome() {
        // Improvement: accept as configurable parameter instead of hard-coded
        System.setProperty("webdriver.chrome.driver", "/apps/webdrivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver useLocalFirefox() {
        // Improvement: accept as configurable parameter instead of hard-coded
        System.setProperty("webdriver.gecko.driver", "/apps/webdrivers/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver useRemoteChrome() throws MalformedURLException {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        URL url;
        if (useDockerNetwork()) {
            url = new URL("http://chrome:4444");
            System.out.println("Using docker network for Remote Chrome WebDriver URL.");
        } else {
            url = new URL("http://localhost:4444");
        }
        WebDriver driver = new RemoteWebDriver(url, options);
        driver.manage().window().maximize();
        return driver;
    }

    public static WebDriver useRemoteFirefox() throws MalformedURLException {
        FirefoxOptions options = new FirefoxOptions();
        options.setHeadless(true);

        URL url;
        if (useDockerNetwork()) {
            url = new URL("http://firefox:4444");
            System.out.println("Using docker network for Remote Firefox WebDriver URL.");
        } else {
            url = new URL("http://localhost:4444");
        }
        WebDriver driver = new RemoteWebDriver(url, options);
        driver.manage().window().maximize();
        return driver;
    }

    private static boolean useDockerNetwork() {
        return Boolean.parseBoolean(System.getProperty("useDockerNetwork"));
    }
}
