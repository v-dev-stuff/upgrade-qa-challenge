package co.legaspi.upgrade;

import org.testng.annotations.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class BuilderTest {

    @Test
    public void buildsDefaultBorrower() {
        Borrower borrower = Borrower.builder().defaults().build();

        System.out.println("borrower = " + borrower);

        assertThat(borrower.getFirstName().length()).isGreaterThanOrEqualTo(3);
        assertThat(borrower.getLastName().length()).isGreaterThanOrEqualTo(3);
        assertThat(borrower.getIndividualIncome()).isGreaterThanOrEqualTo(120_000);
        assertThat(borrower.getAdditionalIncome()).isGreaterThanOrEqualTo(5_000);
        assertThat(borrower.getEmail()).contains("candidate+").contains("@upgrade-challenge.com");
        assertThat(borrower.getAddress().getCity()).isEqualTo("Enumclaw");
    }
}
