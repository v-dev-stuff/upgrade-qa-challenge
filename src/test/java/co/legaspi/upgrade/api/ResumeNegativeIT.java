package co.legaspi.upgrade.api;

import co.legaspi.upgrade.Utils;
import co.legaspi.upgrade.model.ErrorResponse;
import co.legaspi.upgrade.model.LoanResumeRequest;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ResumeNegativeIT {

    @Test
    public void nonExistentLoanAppUuidReturns404() {
        LoanResumeRequest request = LoanResumeRequest.builder().loanAppUuid(Utils.randomUuid()).build();
        Response response = RestClient.INSTANCE.resumeLoan(request);

        ErrorResponse responseAsPojo = response.as(ErrorResponse.class);
        assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_NOT_FOUND);
        assertThat(responseAsPojo.getCodeName()).isEqualTo("MISSING_LOAN_APPLICATION");
    }

    /**
     * "(make sure to use the headers and payload as specified, otherwise the server will respond with a 500)"
     */
    @Test
    public void invalidLoanAppUuidFormatReturns500() {
        LoanResumeRequest request = LoanResumeRequest.builder().loanAppUuid("invalid").build();
        Response response = RestClient.INSTANCE.resumeLoan(request);

        ErrorResponse responseAsPojo = response.as(ErrorResponse.class);
        assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_INTERNAL_SERVER_ERROR);
        assertThat(responseAsPojo.getType()).isEqualTo("ABNORMAL");
    }
}
