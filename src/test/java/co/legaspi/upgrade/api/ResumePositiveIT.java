package co.legaspi.upgrade.api;

import co.legaspi.upgrade.model.LoanResumeRequest;
import co.legaspi.upgrade.model.LoanResumeResponse;
import co.legaspi.upgrade.model.ResetOptions;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ResumePositiveIT {

    @Test(dataProvider = "positiveRequests")
    public void validLoanAppUuidReturns200(LoanResumeRequest request) {
        System.out.println("request = " + request);
        Response response = RestClient.INSTANCE.resumeLoan(request);
        LoanResumeResponse responseAsPojo = response.as(LoanResumeResponse.class);

        System.out.println("responseAsPojo = " + responseAsPojo);

        assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_OK);

        // Just a sample of properties that can be easily parsed (thanks to Jackson) and validated
        assertThat(responseAsPojo.getLoanAppResumptionInfo().getProductType()).isEqualTo("PERSONAL_LOAN");
        assertThat(responseAsPojo.getLoanAppResumptionInfo().getBorrowerResumptionInfo().getFirstName()).isEqualTo("Benjamin");
        assertThat(responseAsPojo.getResetOptions()).contains(ResetOptions.LEAD_SECRET, ResetOptions.WIPE, ResetOptions.DEACTIVATE_USER);

        // TODO: would normally expect the same UUID returned as the one given in the request, but this is a test
        // environment, and the below assertion fails:
        //assertThat(responseAsPojo.getLoanAppResumptionInfo().getLoanAppUuid()).isEqualTo(request.getLoanAppUuid());
    }

    @DataProvider(name = "positiveRequests")
    public Object[][] providePositiveRequests() {
        LoanResumeRequest defaultRequest = LoanResumeRequest.builder().defaults().build();
        LoanResumeRequest dontSkipSideEffects = LoanResumeRequest.builder().defaults().skipSideEffects(false).build();
        return new Object[][]{
                new Object[]{defaultRequest},
                new Object[]{dontSkipSideEffects}
        };
    }
}
