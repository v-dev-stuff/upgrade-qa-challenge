package co.legaspi.upgrade.api;

import co.legaspi.upgrade.Utils;
import co.legaspi.upgrade.model.LoanResumeRequest;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

/**
 * Singleton implemented as an enum.
 */
public enum RestClient {
    INSTANCE;
    private static final String RESUME_ENDPOINT = "https://credapi.credify.tech/api/brfunnelorch/v2/resume/byLeadSecret";

    Response resumeLoan(LoanResumeRequest request) {
        return given()
                .header("x-cf-source-id", "coding-challenge")
                .header("x-cf-corr-id", Utils.randomUuid())
                .header("Content-Type", "application/json")
                .body(request)
                .when()
                .post(RESUME_ENDPOINT)
                .thenReturn()
                ;
    }

}
