#!/usr/bin/env sh

./start_webdriver.sh

# java + maven
docker run --rm --name maven \
  --network tests \
  -v $(pwd):/source \
  -w /source \
  maven:3.8.4-jdk-11 mvn -DuseDockerNetwork --ntp clean verify

./stop_webdriver.sh
